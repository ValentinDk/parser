<?php

namespace Module;

use Factory\ParserFactory;
use Models\Product;
use Models\Url;

class Main
{
    private const FINISH_PARSE = "Completed\n";

    /**
     * @var ParserFactory
     */
    private $factory;

    public function run(): void
    {
        $urls = Url::getUrlsForParse();
        $this->factory = new ParserFactory();

        foreach ($urls as $url) {
            Product::setStatusNotParsedNotProducts($url['url']);

            $this->setDataByUrls($url);
        }
    }

    /**
     * @param array $url
     */
    private function setDataByUrls(array $url): void
    {
        try {
            $parser = $this->factory->getParser($url['parser_name']);
            $parser->parseContentByUrl($url);

            Product::deleteProductsThatHaveNotParsed($url['url']);

            echo self::FINISH_PARSE;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
