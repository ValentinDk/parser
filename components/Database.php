<?php

namespace Components;

class Database extends \PDO
{
    /**
     * @var Database
     */
    private static $connection;

    private function __construct()
    {
        $params = include(ROOT.'/config/db_config.php');

        $dsn = "mysql:host={$params['host']};dbname={$params["dbname"]};charset={$params['charset']}";

        parent::__construct(
            $dsn,
            $params['user'],
            $params['password'],
            $params['options']
        );
    }

    public static function getConnection()
    {
        if(static::$connection === null) {
            static::$connection = new static();
        }

        return static::$connection;
    }
}
