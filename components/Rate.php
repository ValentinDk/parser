<?php

namespace Components;

class Rate
{
    /**
     * @return float
     */
    public static function getRussianRubleRate(): float
    {
        $data = \file_get_contents('http://www.nbrb.by/API/ExRates/Rates/298');

        $data = \json_decode($data);
        $rate = $data->Cur_OfficialRate / 100;

        return $rate;
    }
}