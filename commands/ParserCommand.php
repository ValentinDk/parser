<?php

namespace Commands;

use Module\Main;
use Interfaces\CommandInterfaces;

class ParserCommand implements CommandInterfaces
{
    /**
     * @var Main
     */
    private $output;

    /**
     * @param Main $console
     */
    public function __construct(Main $console)
    {
        $this->output = $console;
    }

    public function execute(): void
    {
        $this->output->run();
    }
}