<?php

namespace Interfaces;

interface CommandInterfaces
{
    public function execute(): void;
}