<?php

namespace Interfaces;

interface ParserInterface
{
    /**
     * @param array $url
     */
    public function parseContentByUrl(array $url): void;
}