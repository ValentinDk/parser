<?php

namespace Models;

use Components\Database;

class Product extends Model
{
    private const DIRECTORY_PICTURE = 'catalog'.DIRECTORY_SEPARATOR.'demo'.DIRECTORY_SEPARATOR.'product'.DIRECTORY_SEPARATOR.'%s.%s';
    private const PATH_TO_PICTURE = ROOT.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'%s';
    private const QUERY_FOR_SEO_URL = 'product_id=%s';

    private $id;
    private $vendorCode;
    private $sku;
    private $upc;
    private $ean;
    private $jan;
    private $isbn;
    private $mpn;
    private $location;
    private $quantity;
    private $stockStatusId;
    private $manufacturerId;
    private $listPicture;
    private $noImage;
    private $extensionImage;
    private $price;
    private $taxClassId;
    private $dateAvailability;
    private $weightClassId;
    private $lengthClassId;
    private $sortOrder;
    private $status;
    private $dateAdded;
    private $dateModified;
    private $languageId;
    private $name;
    private $description;
    private $tag;
    private $metaTitle;
    private $metaDescription;
    private $metaKeyword;
    private $metaH1;
    private $categoryId;
    private $storeId;
    private $layoutId;
    private $seoUrl;
    private $source;

    public function __construct()
    {
        parent::__construct();

        $this->sku = '';
        $this->upc = '';
        $this->ean = '';
        $this->jan = '';
        $this->isbn = '';
        $this->mpn = '';
        $this->location = '';
        $this->manufacturerId = 0;
        $this->taxClassId = 0;
        $this->weightClassId = 1;
        $this->lengthClassId = 1;
        $this->sortOrder = 1;
        $this->status = 1;
        $this->dateAdded = \date('Y-m-d H:i:s');
        $this->dateModified = \date('Y-m-d H:i:s');
        $this->languageId = 1;
        $this->description = '';
        $this->tag = '';
        $this->storeId = 0;
        $this->layoutId = 0;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return null|integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $available
     */
    public function setDateAvailability(string $available): void
    {
        $this->dateAvailability = $available;
    }

    /**
     * @param int $stockStatusId
     */
    public function setStockStatusId(int $stockStatusId): void
    {
        $this->stockStatusId = $stockStatusId;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $vendorCode
     */
    public function setVendorCode(string $vendorCode): void
    {
        $this->vendorCode = $vendorCode;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @param int $categoryId
     */
    public function setCategoryId(int $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @param string $url
     */
    public function setSeoUrl(string $url): void
    {
        $this->seoUrl = $url;
    }

    /**
     * @param array $pictures
     */
    public function setPictures(array $pictures): void
    {
        $this->isPicture($this->id);

        if ($this->noImage && !$this->savePictures($pictures)) {
            $this->listPicture[] = null;
        }
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string $source
     */
    public function setSource(string $source): void
    {
        $this->source = $source;
    }

    /**
     * @param int $id
     */
    private function isPicture(int $id): void
    {
        $query = $this->dataBase->prepare('SELECT image FROM oc_product WHERE product_id = :id');
        $query->execute(['id' => $id]);
        $image = $query->fetch();

        $this->noImage = \is_null($image['image']) || $image['image'] === '';
    }

    /**
     * @param array $pictures
     * @return bool
     */
    private function savePictures(array $pictures): bool
    {
        $isSaveImage = false;

        foreach ($pictures as $picture) {
            $image = $this->resizeImage($picture, 500, 500);

            if ($image) {
                $this->setNamePicture();
                $lastPicture = \end($this->listPicture);

                if (\strcasecmp($this->extensionImage, 'png') === 0) {
                    $isSaveImage = \imagepng($image, \sprintf(self::PATH_TO_PICTURE, $lastPicture));
                } else {
                    $isSaveImage = \imagejpeg($image, \sprintf(self::PATH_TO_PICTURE, $lastPicture));
                }
            }
        }

        return $isSaveImage;
    }

    /**
     * @param string $file
     * @param int $desiredWidth
     * @param int $desiredHeight
     * @return false|resource
     */
    private function resizeImage(string $file, int $desiredWidth, int $desiredHeight)
    {
        $image = false;

        $infoImage = @getimagesize($file);
        list($width, $height) = $infoImage;

        if ($width !== null) {
            $mime = \explode('/', $infoImage['mime']);
            $this->extensionImage = \array_pop($mime);
            $originalSize = ['width' => $width, 'height' => $height];

            if ($width > $desiredWidth || $height > $desiredHeight) {
                $scaleW = $desiredWidth / $width;
                $scaleH = $desiredHeight / $height;
                $scale = \min($scaleW, $scaleH);

                $newSize = ['newWidth' => (int) ($width * $scale), 'newHeight' => (int) ($height * $scale)];
                $image = $this->createImage($file, $newSize, $originalSize);
            } else {
                $newSize = ['newWidth' => $width, 'newHeight' => $height];
                $image = $this->createImage($file, $newSize, $originalSize);
            }
        }

        return $image;
    }

    /**
     * @param string $file
     * @param array $newSize
     * @param array $originalSize
     * @return false|resource
     */
    private function createImage(string $file, array $newSize, array $originalSize)
    {
        if (\strcasecmp($this->extensionImage, 'png') === 0) {
            $src = \imagecreatefrompng($file);
            $image = \imagecreatetruecolor( $newSize['newWidth'], $newSize['newHeight']);

            \imagealphablending($image, false);
            \imagesavealpha($image, true);
        } else {
            $src = \imagecreatefromjpeg($file);
            $image = \imagecreatetruecolor( $newSize['newWidth'], $newSize['newHeight']);
        }

        \imagecopyresampled($image, $src, 0, 0, 0, 0, $newSize['newWidth'], $newSize['newHeight'], $originalSize['width'], $originalSize['height']);
        \imagedestroy($src);

        return $image;
    }

    private function setNamePicture(): void
    {
        $pictureName = \md5(\uniqid(\microtime()));
        $this->listPicture[] = \sprintf(self::DIRECTORY_PICTURE, $pictureName, $this->extensionImage);
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function saveInDataBase(): void
    {
        $isOffer = $this->getOfferById($this->id);

        if ($isOffer) {
            $this->update();
        } else {
            $this->insert();
        }
    }

    protected function update(): void
    {
        try {
            $this->dataBase->beginTransaction();

            if ($this->noImage) {
                $product = $this->dataBase->prepare(
                    'UPDATE oc_product SET
                                quantity = :quantity,
                                stock_status_id = :stockStatusId,
                                image = :picture,
                                price = :price,
                                is_parsed = :isParsed,
                                source = :source,
                                status = :status,
                                date_modified = :dateModified
                          WHERE product_id = :id'
                );
                $product->execute(
                    [
                        'id' => $this->id,
                        'quantity' => $this->quantity,
                        'stockStatusId' => $this->stockStatusId,
                        'picture' => $this->listPicture[0],
                        'price' => $this->price,
                        'isParsed' => 1,
                        'source' => $this->source,
                        'status' => $this->stockStatusId !== 7 ? 0 : 1,
                        'dateModified' => $this->dateModified,
                    ]
                );
            } else {
                $product = $this->dataBase->prepare(
                    'UPDATE oc_product SET
                                quantity = :quantity,
                                stock_status_id = :stockStatusId,
                                price = :price,
                                is_parsed = :isParsed,
                                source = :source,
                                status = :status,
                                date_modified = :dateModified
                          WHERE product_id = :id'
                );
                $product->execute(
                    [
                        'id' => $this->id,
                        'quantity' => $this->quantity,
                        'stockStatusId' => $this->stockStatusId,
                        'price' => $this->price,
                        'isParsed' => 1,
                        'source' => $this->source,
                        'status' => $this->stockStatusId !== 7 ? 0 : 1,
                        'dateModified' => $this->dateModified,
                    ]
                );
            }

            $this->dataBase->commit();
        } catch (\PDOException $e) {
            $this->dataBase->rollBack();

            $now = date("Y-m-d H:i:s");
            file_put_contents(self::SQL_LOG_FILE, $now." ".$e."\r\n", FILE_APPEND);
        }
    }

    protected function insert(): void
    {
        try {
            $this->dataBase->beginTransaction();

            $product = $this->dataBase->prepare(
                'INSERT INTO oc_product (
                                product_id, model, sku, upc, ean, jan, isbn, mpn, location, quantity, stock_status_id,
                                image, manufacturer_id, price, tax_class_id, date_available, weight_class_id,
                                length_class_id, sort_order, status, date_added, date_modified, source
                           )
                           VALUES (
                               :id, :model, :sku, :upc, :ean, :jan, :isbn, :mpn, :location, :quantity, :stockStatusId,
                               :picture, :manufacturerId, :price, :taxClassId, :dateAvailability, :weightClassId, :lengthClassId,
                               :sortOrder, :status, :dateAdded, :dateModified, :source
                           )'
            );
            $product->execute(
                [
                    'id' => $this->id,
                    'model' => $this->vendorCode,
                    'sku' => $this->sku,
                    'upc' => $this->upc,
                    'ean' => $this->ean,
                    'jan' => $this->jan,
                    'isbn' => $this->isbn,
                    'mpn' => $this->mpn,
                    'location' => $this->location,
                    'quantity' => $this->quantity,
                    'stockStatusId' => $this->stockStatusId,
                    'picture' => $this->listPicture[0],
                    'manufacturerId' => $this->manufacturerId,
                    'price' => $this->price,
                    'taxClassId' => $this->taxClassId,
                    'dateAvailability' => $this->dateAvailability,
                    'weightClassId' => $this->weightClassId,
                    'lengthClassId' => $this->lengthClassId,
                    'sortOrder' => $this->sortOrder,
                    'status' => $this->stockStatusId !== 7 ? 0 : 1,
                    'source' => $this->source,
                    'dateAdded' => $this->dateAdded,
                    'dateModified' => $this->dateModified,
                ]
            );
            unset($this->listPicture[0]);

            foreach ($this->listPicture as $picture) {
                $productDescription = $this->dataBase->prepare(
                    'INSERT INTO oc_product_image (product_id, image) VALUES (:productId, :image)'
                );
                $productDescription->execute(
                    [
                        'productId' => $this->id,
                        'image' => $picture,
                    ]
                );
            }

            $productDescription = $this->dataBase->prepare(
                'INSERT INTO oc_product_description (
                                product_id, language_id, name, description, tag, meta_title, meta_description, meta_keyword,
                                meta_h1
                           )
                           VALUES (
                               :id, :languageId, :name, :description, :tag, :metaTitle, :metaDescription, :metaKeyword,
                               :metaH1
                           )'
            );
            $productDescription->execute(
                [
                    'id' => $this->id,
                    'languageId' => $this->languageId,
                    'name' => $this->name,
                    'description' => $this->description,
                    'tag' => $this->tag,
                    'metaTitle' => $this->name,
                    'metaDescription' => $this->name,
                    'metaKeyword' => $this->name,
                    'metaH1' => $this->name,
                ]
            );
            $productToCategory = $this->dataBase->prepare(
                'INSERT INTO oc_product_to_category (product_id, category_id, main_category) VALUES (:id, :categoryId, :mainCategory)'
            );
            $productToCategory->execute(
                [
                    'id' => $this->id,
                    'categoryId' => $this->categoryId,
                    'mainCategory' => 1,
                ]
            );
            $productToStore = $this->dataBase->prepare(
                'INSERT INTO oc_product_to_store (product_id, store_id) VALUES (:id, :storeId)'
            );
            $productToStore->execute(
                [
                    'id' => $this->id,
                    'storeId' => $this->storeId,
                ]
            );
            $productToSeoUrl = $this->dataBase->prepare(
                'INSERT INTO oc_seo_url (store_id, language_id, query, keyword) VALUES (:storeId, :languageId, :query, :keyword)'
            );
            $productToSeoUrl->execute(
                [
                    'storeId' => $this->storeId,
                    'languageId' => $this->languageId,
                    'query' => \sprintf(self::QUERY_FOR_SEO_URL, $this->id),
                    'keyword' => $this->seoUrl,
                ]
            );
            $this->dataBase->commit();
        } catch (\PDOException $e) {
            $this->dataBase->rollBack();

            $now = \date("Y-m-d H:i:s");
            \file_put_contents(self::SQL_LOG_FILE, $now." ".$e."\r\n", FILE_APPEND);
        }
    }

    /**
     * @param int $id
     * @return array|false
     */
    public function getOfferById(int $id)
    {
        $query = $this->dataBase->prepare('SELECT * FROM oc_product WHERE product_id = :id');
        $query->execute(['id' => $id]);
        $offer = $query->fetch();

        return $offer;
    }

    /**
     * @param string $vendorCode
     * @return mixed
     */
    public static function getProductByVendorCode(string $vendorCode)
    {
        $dataBase = Database::getConnection();

        $query = $dataBase->prepare('SELECT * FROM oc_product WHERE model = :vendorCode');
        $query->execute(['vendorCode' => $vendorCode]);
        $product = $query->fetch();

        return $product;
    }

    /**
     * @return array|false
     */
    public static function getMaxId()
    {
        $dataBase = Database::getConnection();

        $query = $dataBase->query('SELECT MAX(product_id) as max FROM oc_product');
        $id = $query->fetch();

        return $id;
    }

    /**
     * @param string $vendorCode
     * @return string|false
     */
    public static function getProductNameByVendorCode(string $vendorCode)
    {
        $dataBase = Database::getConnection();

        $query = $dataBase->prepare('SELECT `name` FROM oc_name_and_image_for_product WHERE article LIKE :vendorCode');
        $query->execute(['vendorCode' => "%$vendorCode"]);
        $product = $query->fetch();

        return $product ? $product['name'] : false;
    }

    /**
     * @param string $vendorCode
     * @return string|false
     */
    public function getProductImageByVendorCode(string $vendorCode)
    {
        $this->isPicture($this->id);

        if ($this->noImage) {
            $query = $this->dataBase->prepare('SELECT image FROM oc_name_and_image_for_product WHERE article LIKE :vendorCode');
            $query->execute(['vendorCode' => "%$vendorCode"]);
            $product = $query->fetch();

            return empty($product) || is_null($product['image']) ? false : $this->listPicture[] = $product['image'];
        }

        return true;
    }

    /**
     * @param string $vendorCode
     * @return false|string
     */
    public function getEndOfVendorCode(string $vendorCode)
    {
        $disassembledVendorCode = \explode('-', $vendorCode);

        if (\count($disassembledVendorCode) > 2) {
            $endOfVendorCode = $disassembledVendorCode[\count($disassembledVendorCode) - 2] .'-'. \end($disassembledVendorCode);
        } else {
            $endOfVendorCode = false;
        }

        return $endOfVendorCode;
    }

    /**
     * @param string $colorCode
     * @return false|string
     */
    public function setProductImageByColorCode(string $colorCode)
    {
        $this->isPicture($this->id);

        if ($this->noImage) {
            $query = $this->dataBase->prepare('SELECT image FROM oc_color_code_and_image_for_product WHERE color_code = :colorCode');
            $query->execute(['colorCode' => $colorCode]);
            $product = $query->fetch();

            return empty($product) ? false : $this->listPicture[] = $product['image'];
        }

        return true;
    }

    /**
     * @param string $colorCode
     * @return false|string
     */
    public function getEndOfArticleByColorCode(string $colorCode)
    {
        $query = $this->dataBase->prepare('SELECT end_of_article FROM oc_color_code_and_image_for_product WHERE color_code = :colorCode');
        $query->execute(['colorCode' => $colorCode]);
        $product = $query->fetch();

        return empty($product) ? false : $product['end_of_article'];
    }

    /**
     * @param string $source
     */
    public static function setStatusNotParsedNotProducts(string $source): void
    {
        $dataBase = Database::getConnection();

        $query = $dataBase->prepare('UPDATE oc_product SET is_parsed = :isParsed WHERE source = :source');
        $query->execute(
            [
                'isParsed' => 0,
                'source' => $source,
            ]
        );
    }

    /**
     * @param string $source
     */
    public static function deleteProductsThatHaveNotParsed(string $source): void
    {
        $dataBase = Database::getConnection();
        $products = self::getProductsThatHaveNotParsed($source);

        foreach ($products as $product) {
            $queryProductTable = $dataBase->prepare('UPDATE oc_product SET status = :status WHERE product_id = :id');
            $queryProductTable->execute(
                [
                    'id' => $product['product_id'],
                    'status' => 0,
                ]
            );

            echo 'Product is hidden: ' . $product['model'] . "\n";
        }
    }

    /**
     * @param string $source
     * @return array
     */
    private static function getProductsThatHaveNotParsed(string $source): array
    {
        $dataBase = Database::getConnection();

        $query = $dataBase->prepare('SELECT `product_id`, `model` FROM oc_product WHERE is_parsed = :isParsed AND source = :source');
        $query->execute(
            [
                'isParsed' => 0,
                'source' => $source,
            ]
        );
        $products = $query->fetchAll();

        return $products;
    }
}
