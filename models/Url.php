<?php

namespace Models;

use Components\Database;

class Url
{
    /**
     * @return array
     */
    public static function getUrlsForParse(): array
    {
        $dataBase = Database::getConnection();

        $query = $dataBase->query('SELECT * FROM oc_url_for_parse WHERE status = 1');
        $urls = $query->fetchAll();

        return $urls;
    }
}