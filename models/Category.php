<?php

namespace Models;

use Components\Database;

class Category extends Model
{
    private const DIRECTORY_PICTURE = 'catalog'.DIRECTORY_SEPARATOR.'demo'.DIRECTORY_SEPARATOR.'%s.%s';
    private const PATH_TO_PICTURE = ROOT.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'%s';
    private const QUERY_FOR_SEO_URL = 'category_id=%s';

    private $top;
    private $status;
    private $dateAdded;
    private $dateModified;
    private $languageId;
    private $description;
    private $metaTitle;
    private $metaDescription;
    private $metaKeyword;
    private $metaH1;
    private $storeId;
    private $layoutId;
    private $id;
    private $parentId;
    private $name;
    private $seoUrl;
    private $image;
    private $noImage;
    private $extensionImage;

    public function __construct()
    {
        parent::__construct();

        $this->top = 0;
        $this->status = 1;
        $this->dateAdded = date('Y-m-d H:i:s');
        $this->dateModified = date('Y-m-d H:i:s');
        $this->languageId = 1;
        $this->description = '';
        $this->metaTitle = '';
        $this->metaDescription = '';
        $this->metaKeyword = '';
        $this->metaH1 = '';
        $this->storeId = 0;
        $this->layoutId = 0;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return null|integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $parentId
     */
    public function setParentId(int $parentId): void
    {
        $this->parentId = $parentId;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $seoUrl
     */
    public function setSeoUrl(string $seoUrl): void
    {
        $this->seoUrl = $seoUrl;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->isImage($this->id);

        if ($this->noImage && !$this->saveImage($image)) {
            $this->image = null;
        }
    }

    /**
     * @param int $id
     */
    private function isImage(int $id): void
    {
        $query = $this->dataBase->prepare('SELECT image FROM oc_category WHERE category_id = :id');
        $query->execute(['id' => $id]);
        $image = $query->fetch();

        $this->noImage = is_null($image['image']) || $image['image'] === '';
    }

    /**
     * @param string $picture
     * @return bool
     */
    private function saveImage(string $picture): bool
    {
        $saveImage = false;
        $content = $this->resizeImage($picture, 240, 240);

        if ($content) {
            $this->setNameImage();

            if (\strcasecmp($this->extensionImage, 'png') === 0) {
                $saveImage = \imagepng($content, \sprintf(self::PATH_TO_PICTURE, $this->image));
            } else {
                $saveImage = \imagejpeg($content, \sprintf(self::PATH_TO_PICTURE, $this->image));
            }
        }

        return $saveImage;
    }

    /**
     * @param string $file
     * @param int $desiredWidth
     * @param int $desiredHeight
     * @return false|resource
     */
    private function resizeImage(string $file, int $desiredWidth, int $desiredHeight)
    {
        $image = false;

        $infoImage = @getimagesize($file);
        list($width, $height) = $infoImage;

        if ($width !== null) {
            $mime = \explode('/', $infoImage['mime']);
            $this->extensionImage = \array_pop($mime);
            $originalSize = ['width' => $width, 'height' => $height];

            if ($width > $desiredWidth || $height > $desiredHeight) {
                $scaleW = $desiredWidth / $width;
                $scaleH = $desiredHeight / $height;
                $scale = \min($scaleW, $scaleH);

                $newSize = ['newWidth' => (int) ($width * $scale), 'newHeight' => (int) ($height * $scale)];
                $image = $this->createImage($file, $newSize, $originalSize);
            } else {
                $newSize = ['newWidth' => $width, 'newHeight' => $height];
                $image = $this->createImage($file, $newSize, $originalSize);
            }
        }

        return $image;
    }

    /**
     * @param string $file
     * @param array $newSize
     * @param array $originalSize
     * @return false|resource
     */
    private function createImage(string $file, array $newSize, array $originalSize)
    {
        if (\strcasecmp($this->extensionImage, 'png') === 0) {
            $src = \imagecreatefrompng($file);
            $image = \imagecreatetruecolor( $newSize['newWidth'], $newSize['newHeight']);

            \imagealphablending($image, false);
            \imagesavealpha($image, true);
        } else {
            $src = \imagecreatefromjpeg($file);
            $image = \imagecreatetruecolor( $newSize['newWidth'], $newSize['newHeight']);
        }

        \imagecopyresampled($image, $src, 0, 0, 0, 0, $newSize['newWidth'], $newSize['newHeight'], $originalSize['width'], $originalSize['height']);
        \imagedestroy($src);

        return $image;
    }

    private function setNameImage(): void
    {
        $pictureName = \md5(\uniqid(\microtime()));
        $this->image = \sprintf(self::DIRECTORY_PICTURE, $pictureName, $this->extensionImage);
    }

    public function saveInDataBase(): void
    {
        $isCategory = $this->getCategoryById($this->id);

        if ($isCategory) {
            $this->update();
        } else {
            $this->insert();
        }
    }

    protected function update(): void
    {
        try {
            $this->dataBase->beginTransaction();

            if ($this->noImage) {
                $category = $this->dataBase->prepare(
                    'UPDATE oc_category SET image = :image, date_modified = :dateModified WHERE category_id = :id'
                );
                $category->execute(
                    [
                        'id' => $this->id,
                        'image' => $this->image,
                        'dateModified' => $this->dateModified < date('Y-m-d') ? date('Y-m-d') : $this->dateModified,
                    ]
                );
            } else {
                $category = $this->dataBase->prepare(
                    'UPDATE oc_category SET date_modified = :dateModified WHERE category_id = :id'
                );
                $category->execute(
                    [
                        'id' => $this->id,
                        'dateModified' => $this->dateModified < date('Y-m-d') ? date('Y-m-d') : $this->dateModified,
                    ]
                );
            }

            $this->dataBase->commit();
        } catch (\PDOException $e) {
            $this->dataBase->rollback();

            $now = date("Y-m-d H:i:s");
            file_put_contents(self::SQL_LOG_FILE, $now." ".$e."\r\n", FILE_APPEND);
        }
    }

    protected function insert(): void
    {
        try {
            $this->dataBase->beginTransaction();

            $category = $this->dataBase->prepare(
                'INSERT INTO oc_category (category_id, image, parent_id, top, status, date_added, date_modified)
                          VALUES (:id, :image, :parentId, :top, :status, :dateAdded, :dateModified)'
            );
            $category->execute(
                [
                    'id' => $this->id,
                    'image' => $this->image,
                    'parentId' => $this->parentId,
                    'top' => $this->top,
                    'status' => $this->status,
                    'dateAdded' => $this->dateAdded,
                    'dateModified' => $this->dateModified < date('Y-m-d H:i:s') ? date('Y-m-d H:i:s') : $this->dateModified,
                ]
            );
            $categoryDescription = $this->dataBase->prepare(
                'INSERT INTO oc_category_description (
                             category_id, language_id, name, description, meta_title, meta_description, meta_keyword,meta_h1)
                          VALUES (:id, :languageId, :name, :description, :metaTitle, :metaDescription, :metaKeyword, :metaH1)'
            );
            $categoryDescription->execute(
                [
                    'id' => $this->id,
                    'name' => $this->name,
                    'languageId' => $this->languageId,
                    'description' => $this->description,
                    'metaTitle' => $this->metaTitle,
                    'metaDescription' => $this->metaDescription,
                    'metaKeyword' => $this->metaKeyword,
                    'metaH1' => $this->metaH1,
                ]
            );
            // MySQL Hierarchical Data Closure Table Pattern
            $level = 0;

            $categoryPaths = $this->dataBase->prepare(
                'SELECT * FROM oc_category_path WHERE category_id = :parentId ORDER BY level ASC'
            );
            $categoryPaths->execute(
                [
                    'parentId' => $this->parentId,
                ]
            );

            foreach ($categoryPaths->fetchAll() as $category) {
                $categoryPath = $this->dataBase->prepare(
                    'INSERT INTO oc_category_path SET category_id = :id, path_id = :pathId, level = :level'
                );
                $categoryPath->execute(
                    [
                        'id' => $this->id,
                        'pathId' => $category['path_id'],
                        'level' => $level,
                    ]
                );
                $level++;
            }

            $categoryPath = $this->dataBase->prepare(
                'INSERT INTO oc_category_path SET category_id = :id, path_id = :pathId, level = :level'
            );
            $categoryPath->execute(
                [
                    'id' => $this->id,
                    'pathId' => $this->id,
                    'level' => $level,
                ]
            );
            $categoryToStore = $this->dataBase->prepare(
                'INSERT INTO oc_category_to_store (category_id, store_id) VALUES (:id, :storeId)'
            );
            $categoryToStore->execute(
                [
                    'id' => $this->id,
                    'storeId' => $this->storeId,
                ]
            );
            $categoryToSeoUrl = $this->dataBase->prepare(
                'INSERT INTO oc_seo_url (store_id, language_id, query, keyword) VALUES (:storeId, :languageId, :query, :keyword)'
            );
            $categoryToSeoUrl->execute(
                [
                    'storeId' => $this->storeId,
                    'languageId' => $this->languageId,
                    'query' => \sprintf(self::QUERY_FOR_SEO_URL, $this->id),
                    'keyword' => $this->seoUrl,
                ]
            );
            $this->dataBase->commit();
        } catch (\PDOException $e) {
            $this->dataBase->rollBack();

            $now = \date("Y-m-d H:i:s");
            \file_put_contents(self::SQL_LOG_FILE, $now." ".$e."\r\n", FILE_APPEND);
        }
    }

    /**
     * @param int $id
     */
    public function setEquipment(int $id): void
    {
        $query = $this->dataBase->prepare(
            'UPDATE oc_category SET is_equipment = :isEquipment WHERE category_id = :id'
        );
        $query->execute(
            [
                'id' => $id,
                'isEquipment' => 1,
            ]
        );
    }

    /**
     * @param int $id
     * @return array|false
     */
    public static function getCategoryById(int $id)
    {
        $dataBase = Database::getConnection();

        $query = $dataBase->prepare('SELECT * FROM oc_category WHERE category_id = :id');
        $query->execute(['id' => $id]);
        $category = $query->fetch();

        return $category;
    }

    /**
     * @param string $name
     * @param int $parentId
     * @return array|false
     */
    public static function getCategoryByNameAndParentId(string $name, int $parentId)
    {
        $dataBase = Database::getConnection();

        $query = $dataBase->prepare(
            'SELECT * FROM oc_category_description
                      LEFT JOIN oc_category USING(category_id)
                      WHERE oc_category.parent_id = :parentId AND oc_category_description.name = :name'
        );
        $query->execute(['parentId' => $parentId, 'name' => $name]);
        $category = $query->fetch();

        return $category;
    }

    /**
     * @return array|false
     */
    public static function getMaxId()
    {
        $dataBase = Database::getConnection();

        $query = $dataBase->query('SELECT MAX(category_id) as max FROM oc_category');
        $id = $query->fetch();

        return $id;
    }

    /**
     * @param int $parentId
     * @return array|false
     */
    public static function getCategoriesByParentCategoryId(int $parentId)
    {
        $dataBase = Database::getConnection();

        $query = $dataBase->prepare(
            'SELECT * FROM oc_category_description
                      LEFT JOIN oc_category USING (category_id)
                      WHERE parent_id = :parentId'
        );
        $query->execute(['parentId' => $parentId]);
        $categories = $query->fetchAll();

        return $categories;
    }
}