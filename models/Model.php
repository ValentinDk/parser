<?php

namespace Models;

use Components\Database;

abstract class Model
{
    protected const SQL_LOG_FILE = ROOT.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'sqlLogs.txt';

    protected $dataBase;

    public function __construct()
    {
        $this->dataBase = Database::getConnection();
    }

    abstract public function saveInDataBase(): void;

    abstract protected function update(): void;

    abstract protected function insert(): void;
}