<?php

use Commands\ParserCommand;
use Module\Main;

error_reporting(E_ALL);
ini_set('display_errors', 1);

define('ROOT', dirname(__FILE__));

include_once(ROOT.'/vendor/autoload.php');

$command = new ParserCommand(new Main());
$command->execute();