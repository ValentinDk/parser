<?php

namespace Factory;

class ParserFactory
{
    const CLASS_PATH = 'Parsers\\%sParser';
    const FILE_PATH = ROOT.DIRECTORY_SEPARATOR.'parsers'.DIRECTORY_SEPARATOR.'%sParser.php';

    /**
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    public function getParser(string $name)
    {
        $parserName = \ucfirst($name);
        $className = \sprintf(self::CLASS_PATH, $parserName);
        $fileName = \sprintf(self::FILE_PATH, $parserName);

        if (file_exists($fileName)) {
            return new $className;
        } else {
            throw new \Exception('Error parser name');
        }
    }
}