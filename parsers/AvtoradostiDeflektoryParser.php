<?php

namespace Parsers;

use Components\ConversionToSeoUrl;
use Interfaces\ParserInterface;
use Models\Category;
use Models\Product;

class AvtoradostiDeflektoryParser implements ParserInterface
{
    private const URL_SITE = 'https://avtoradosti.by%s';
    private const PATTERN_FOR_PAGE = '@link_pos_last" href="(.+?)">&rarr;@';
    private const PATTERN_FOR_PRODUCTS = '@product-list__title[\w\W]*?">(.+?)</a>[\w\W]*?title="Арт:">(.+?)<[\w\W]*?state_val[\w]+">(.+?)<[\w\W]*?current-price">([\d]+)@';
    private const STATUS_ID_FOR_PRODUCT_IN_STOCK = 7;
    private const STATUS_ID_FOR_PRODUCT_OUT_OF_STOCK = 8;
    private const SEO_URL = '%s-%s';
    private const DISPLAY_PRODUCT = "product %s id %s\n";

    private $maxCategoryId;
    private $maxProductId;
    private $markup;
    private $url;
    private $parentIdForImage;
    private $categories;

    public function parseContentByUrl(array $url): void
    {
        $this->maxCategoryId = Category::getMaxId();
        $this->maxProductId = Product::getMaxId();
        $this->markup = $url['markup'];
        $this->url = $url['url'];
        $this->parentIdForImage = $url['parent_id'];
        $this->categories = Category::getCategoriesByParentCategoryId($url['parent_id']);

        $this->setProductsFromAllPages($url['url'], $url['parent_id']);
    }

    /**
     * @param string $url
     * @param int $categoryId
     */
    private function setProductsFromAllPages(string $url, int $categoryId): void
    {
        $htmlProducts = $this->getContentByUrl($url, self::PATTERN_FOR_PRODUCTS);
        $this->setProducts($htmlProducts, $categoryId);

        $nextPage = $this->getContentByUrl($url, self::PATTERN_FOR_PAGE);

        if (!empty($nextPage[0])) {
            $this->setProductsFromAllPages(\sprintf(self::URL_SITE, $nextPage[1][0]), $categoryId);
        }
    }

    /**
     * @param string $url
     * @param string $pattern
     * @return array
     */
    private function getContentByUrl(string $url, string $pattern): array
    {
        usleep(1000000);

        $ch = \curl_init();
        \curl_setopt($ch, CURLOPT_URL, $url);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $content = \curl_exec($ch);
        \curl_close($ch);

        \preg_match_all($pattern, $content, $result, PREG_PATTERN_ORDER);

        return $result;
    }

    /**
     * @param array $htmlProducts
     * @param int $categoryId
     */
    private function setProducts(array $htmlProducts, int $categoryId): void
    {
        foreach ($htmlProducts[1] as $item => $name) {
            $product = new Product();

            $existingProduct = Product::getProductByVendorCode($htmlProducts[2][$item]);

            if ($existingProduct) {
                $product->setId($existingProduct['product_id']);
                $product->setSource($this->url);
            } else {
                $product->setId(++$this->maxProductId['max']);
                $product->setName($name);
                $product->setDateAvailability(\date('Y-m-d'));
                $product->setCategoryId($this->getCategoryByProductNameAndParentId($name, $categoryId));
                $product->setDescription('');
                $product->setSource($this->url);

                $seoUrl = ConversionToSeoUrl::conversion($name);
                $product->setSeoUrl(\sprintf(self::SEO_URL, $seoUrl, $product->getId()));
            }

            if (\strpos($htmlProducts[3][$item], 'В наличии') === false) {
                $stockStatusId = self::STATUS_ID_FOR_PRODUCT_OUT_OF_STOCK;
                $product->setQuantity(0);
            } else {
                $stockStatusId = self::STATUS_ID_FOR_PRODUCT_IN_STOCK;
                $product->setQuantity(100);
            }

            $product->setVendorCode($htmlProducts[2][$item]);
            $product->setStockStatusId($stockStatusId);
            $product->setPictures(['https://images.by.prom.st/104824318_w275_h275_deflektor-kapota-bmw.jpg']);

            $price = (int) $htmlProducts[4][$item];
            $priceWithMarkup = $price * $this->markup;
            $roundedPrice = \round($priceWithMarkup / 5) * 5;

            $product->setPrice($roundedPrice < 20 ? 20 : $roundedPrice);

            $product->saveInDataBase();

            echo \sprintf(self::DISPLAY_PRODUCT, $name, $product->getId());
        }
    }

    /**
     * @param string $name
     * @param int $categoryId
     * @return int
     */
    private function getCategoryByProductNameAndParentId(string $name, int $categoryId): int
    {
        foreach ($this->categories as $category) {
            if (\stripos($name, $category['name']) !== false) {
                return (int) $category['category_id'];
            }
        }

        return $categoryId;
    }
}