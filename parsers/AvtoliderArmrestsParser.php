<?php

namespace Parsers;

use Components\ConversionToSeoUrl;
use Components\Rate;
use Interfaces\ParserInterface;
use Models\Category;
use Models\Product;

class AvtoliderArmrestsParser implements ParserInterface
{
    private const URL_SITE = 'https://avtolider1.ru%s';
    private const PATTERN_FOR_CATEGORIES_BASED_PRODUCTS = '@href="(.+)" [\w="/ ]+itemprop="url">[\s]+<span itemprop="title">(.+)</span>@u';
    private const PATTERN_FOR_CATEGORIES = '@href="([\/\w]+)"[>\s<\w="]+([\/\w-.]+)"[>\/<\w\s]+<span>(.+)<\/span>@';
    private const PATTERN_FOR_PAGE = '@class="page_item"[\w\s="]+([\/\w?=]+)"@';
    private const PATTERN_FOR_DEMO_PRODUCTS = '@name_elem"><a href="([\w\/]+)">[\w\W]*?</span>[\s]+<span>(.+?)<\/@';
    private const PATTERN_DATA_FOR_PRODUCTS = '@discribe"><h1>(.+,)[\W\w]+?offer-code="true">(.+)<[\W\w]+?price="(\d+)">[\w\W]*?tab_cont">([\w\W]*?)<div class="prop_list"@';
    private const PATTERN_FOR_PRODUCTS = '@max-quantity="(\d+)"[\w\W]*?store="instock"[\w\W]*?color-value="(.*?)"[\w\W]*?title="(.+)"@';
    private const STATUS_ID_FOR_PRODUCT_IN_STOCK = 7;
    private const STATUS_ID_FOR_PRODUCT_OUT_OF_STOCK = 8;
    private const SEO_URL = '%s-%s';
    private const PRODUCT_NAME = "%s %s";
    private const DISPLAY_CATEGORY = "category %s id %s\n";
    private const DISPLAY_PRODUCT = "product %s id %s\n";

    private $maxCategoryId;
    private $maxProductId;
    private $markup;
    private $stockStatus;
    private $url;

    /**
     * @param array $url
     */
    public function parseContentByUrl(array $url): void
    {
        $this->maxCategoryId = Category::getMaxId();
        $this->maxProductId = Product::getMaxId();
        $this->markup = $url['markup'];
        $this->url = $url['url'];

        $categoryInfo = $this->createCategoryBasedProducts($url['url'], $url['parent_id']);
        $this->parse($categoryInfo[0], $categoryInfo[1]);
    }

    /**
     * @param string $url
     * @param int $parentId
     * @return array
     */
    private function createCategoryBasedProducts(string $url, int $parentId): array
    {
        $htmlCategoryBasedProducts = $this->getContentByUrl($url, self::PATTERN_FOR_CATEGORIES_BASED_PRODUCTS);
        $htmlCategory[] = \array_pop($htmlCategoryBasedProducts[1]);
        $htmlCategory[] = \array_pop($htmlCategoryBasedProducts[2]);

        $categoryBasedProducts = $this->createCategory($htmlCategory[1], $parentId);
        $categoryInfo = [$htmlCategory[0], $categoryBasedProducts->getId()];

        return $categoryInfo;
    }

    /**
     * @param string $url
     * @param int $parentId
     */
    private function parse(string $url, int $parentId): void
    {
        $this->setCategories($url, $parentId);
        $this->setProductsFromAllPages($url, $parentId);
    }

    /**
     * @param string $url
     * @param int $parentId
     */
    private function setCategories(string $url, int $parentId): void
    {
        $htmlCategories = $this->getContentByUrl(\sprintf(self::URL_SITE, $url), self::PATTERN_FOR_CATEGORIES);

        foreach ($htmlCategories[1] as $item => $categoryUrl) {
            $image = \sprintf(self::URL_SITE, $htmlCategories[2][$item]);
            $category = $this->createCategory($htmlCategories[3][$item], $parentId, $image);

            $this->parse($categoryUrl, $category->getId());
        }
    }

    /**
     * @param string $url
     * @param string $pattern
     * @return array
     */
    private function getContentByUrl(string $url, string $pattern): array
    {
        $ch = \curl_init();
        \curl_setopt($ch, CURLOPT_URL, $url);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $content = \curl_exec($ch);
        \curl_close($ch);

        \preg_match_all($pattern, $content, $result, PREG_PATTERN_ORDER);

        return $result;
    }

    /**
     * @param string $name
     * @param int $parentId
     * @param string $image
     * @return Category
     */
    private function createCategory(string $name, int $parentId, string $image = ''): Category
    {
        $category = new Category();

        $existingCategory = Category::getCategoryByNameAndParentId($name, $parentId);

        if ($existingCategory) {
            $category->setId($existingCategory['category_id']);
        } else {
            $category->setId(++$this->maxCategoryId['max']);
            $category->setParentId($parentId);
            $category->setName(\strtoupper($name));

            $seoUrl = ConversionToSeoUrl::conversion($name);
            $category->setSeoUrl(\sprintf(self::SEO_URL, $seoUrl, $category->getId()));
        }

        $category->setImage($image);

        $category->saveInDataBase();

        echo \sprintf(self::DISPLAY_CATEGORY, $name, $category->getId());

        return $category;
    }

    /**
     * @param string $url
     * @param int $categoryId
     */
    private function setProductsFromAllPages(string $url, int $categoryId): void
    {
        $htmlDemoProducts = $this->getContentByUrl(\sprintf(self::URL_SITE, $url), self::PATTERN_FOR_DEMO_PRODUCTS);
        $this->setProductsByDemoProducts($htmlDemoProducts, $categoryId);

        $pages = $this->getContentByUrl(\sprintf(self::URL_SITE, $url), self::PATTERN_FOR_PAGE);

        foreach ($pages[1] as $item => $pageUrl) {
            $htmlDemoProducts = $this->getContentByUrl(\sprintf(self::URL_SITE, $pageUrl), self::PATTERN_FOR_DEMO_PRODUCTS);
            $this->setProductsByDemoProducts($htmlDemoProducts, $categoryId);
        }
    }

    /**
     * @param array $htmlDemoProducts
     * @param int $categoryId
     */
    private function setProductsByDemoProducts(array $htmlDemoProducts, int $categoryId): void
    {
        foreach ($htmlDemoProducts[1] as $item => $productUrl) {
            if (\strpos($htmlDemoProducts[2][$item], 'наличии') === false) {
                $this->stockStatus = self::STATUS_ID_FOR_PRODUCT_OUT_OF_STOCK;
            } else {
                $this->stockStatus = self::STATUS_ID_FOR_PRODUCT_IN_STOCK;
            }

            $dataForProducts = $this->getContentByUrl(\sprintf(self::URL_SITE, $productUrl), self::PATTERN_DATA_FOR_PRODUCTS);
            $htmlProducts = $this->getContentByUrl(\sprintf(self::URL_SITE, $productUrl), self::PATTERN_FOR_PRODUCTS);

            $this->setProduct($dataForProducts, $htmlProducts, $categoryId);
        }
    }

    /**
     * @param array $dataForProducts
     * @param array $htmlProducts
     * @param int $categoryId
     */
    private function setProduct(array $dataForProducts, array $htmlProducts, int $categoryId): void
    {
        $startOfArticle = $this->getStartArticle($dataForProducts);

        foreach ($htmlProducts[1] as $item => $name) {
            $product = new Product();

            $endOfArticle = $product->getEndOfArticleByColorCode($htmlProducts[2][$item]);
            $fullArticle = $startOfArticle . $endOfArticle;

            $existingProduct = Product::getProductByVendorCode($fullArticle);

            if ($existingProduct) {
                $product->setId($existingProduct['product_id']);
                $product->setVendorCode($existingProduct['model']);
                $product->setSource($this->url);
            } else {
                $product->setId(++$this->maxProductId['max']);
                $product->setName(\sprintf(self::PRODUCT_NAME, $dataForProducts[1][0], $htmlProducts[3][$item]));
                $product->setDateAvailability(\date('Y-m-d'));
                $product->setCategoryId($categoryId);
                $product->setDescription(\strip_tags($dataForProducts[4][0]));
                $product->setVendorCode($fullArticle);
                $product->setSource($this->url);

                $seoUrl = ConversionToSeoUrl::conversion($dataForProducts[1][0] . $htmlProducts[3][$item]);
                $product->setSeoUrl(\sprintf(self::SEO_URL, $seoUrl, $product->getId()));
            }

            $product->setQuantity($this->stockStatus === self::STATUS_ID_FOR_PRODUCT_IN_STOCK ? (int) $htmlProducts[1][$item] : 0);
            $product->setStockStatusId($this->stockStatus);
            $product->setProductImageByColorCode($htmlProducts[2][$item]);

            $price = (int) $dataForProducts[3][0];
            $priceWithMarkup = $price * $this->markup;
            $priceBYN = \round($priceWithMarkup * Rate::getRussianRubleRate() / 5) * 5;

            $product->setPrice($priceBYN < 20 ? 20 : $priceBYN);

            $product->saveInDataBase();

            echo \sprintf(self::DISPLAY_PRODUCT, $fullArticle, $product->getId());
        }
    }

    /**
     * @param array $dataForProducts
     * @return string
     */
    private function getStartArticle(array $dataForProducts): string
    {
        $startOfArticle = \explode('-', $dataForProducts[2][0]);
        \array_pop($startOfArticle);
        $startOfArticle = \implode('-', $startOfArticle);

        return $startOfArticle;
    }
}