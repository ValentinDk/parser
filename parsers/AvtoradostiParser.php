<?php

namespace Parsers;

use Components\ConversionToSeoUrl;
use Interfaces\ParserInterface;
use Models\Category;
use Models\Product;

class AvtoradostiParser implements ParserInterface
{
    private const URL_SITE = 'https://avtoradosti.by%s';
    private const PATTERN_FOR_CATEGORIES = '@product-groups-list__title" href="(.+?)">(.+?)<@';
    private const PATTERN_FOR_PAGE = '@link_pos_last" href="(.+?)">&rarr;@';
    private const PATTERN_FOR_PRODUCTS = '@product-list__title[\w\W]*?">(.+?)</a>[\w\W]*?title="Арт:">(.+?)<[\w\W]*?state_val[\w]+">(.+?)<[\w\W]*?current-price">([\d]+)@';
    private const STATUS_ID_FOR_PRODUCT_IN_STOCK = 7;
    private const STATUS_ID_FOR_PRODUCT_OUT_OF_STOCK = 8;
    private const SEO_URL = '%s-%s';
    private const DISPLAY_CATEGORY = "category %s id %s\n";
    private const DISPLAY_PRODUCT = "product %s id %s\n";

    private $maxCategoryId;
    private $maxProductId;
    private $markup;
    private $url;
    private $parentIdForImage;

    public function parseContentByUrl(array $url): void
    {
        $this->maxCategoryId = Category::getMaxId();
        $this->maxProductId = Product::getMaxId();
        $this->markup = $url['markup'];
        $this->url = $url['url'];
        $this->parentIdForImage = $url['parent_id'];

        $this->parse($url['url'], $url['parent_id']);
    }

    /**
     * @param string $url
     * @param int $parentId
     */
    private function parse(string $url, int $parentId): void
    {
        $htmlCategories = $this->getContentByUrl($url, self::PATTERN_FOR_CATEGORIES);

        foreach ($htmlCategories[1] as $item => $categoryUrl) {
            $category = $this->createCategory($htmlCategories[2][$item], $parentId);

            $this->setProductsFromAllPages(\sprintf(self::URL_SITE, $categoryUrl), $category->getId());
        }
    }

    /**
     * @param string $url
     * @param string $pattern
     * @return array
     */
    private function getContentByUrl(string $url, string $pattern): array
    {
        usleep(1000000);

        $ch = \curl_init();
        \curl_setopt($ch, CURLOPT_URL, $url);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $content = \curl_exec($ch);
        \curl_close($ch);

        \preg_match_all($pattern, $content, $result, PREG_PATTERN_ORDER);

        return $result;
    }

    /**
     * @param string $name
     * @param int $parentId
     * @param string $image
     * @return Category
     */
    private function createCategory(string $name, int $parentId, string $image = ''): Category
    {
        $category = new Category();

        if ($name === 'Lada') {
            $name = 'LADA (ВАЗ)';
        } elseif ($name === 'SsangYong') {
            $name = 'Ssang Yong';
        } elseif ($name === 'Mercedes-Benz') {
            $name = 'MERCEDES BENZ';
        } elseif ($name === 'Great') {
            $name = 'GREAT WALL';
        } elseif ($name === 'Uaz' || $name === 'UAZ') {
            $name = 'УАЗ';
        }

        $existingCategory = Category::getCategoryByNameAndParentId($name, $parentId);

        if ($existingCategory) {
            $category->setId($existingCategory['category_id']);
        } else {
            $category->setId(++$this->maxCategoryId['max']);
            $category->setParentId($parentId);
            $category->setName(\strtoupper($name));

            $seoUrl = ConversionToSeoUrl::conversion($name);
            $category->setSeoUrl(\sprintf(self::SEO_URL, $seoUrl, $category->getId()));
        }

        $category->setImage($image);

        $category->saveInDataBase();

        echo \sprintf(self::DISPLAY_CATEGORY, $name, $category->getId());

        return $category;
    }

    /**
     * @param string $url
     * @param int $categoryId
     */
    private function setProductsFromAllPages(string $url, int $categoryId): void
    {
        $htmlProducts = $this->getContentByUrl($url, self::PATTERN_FOR_PRODUCTS);
        $this->setProducts($htmlProducts, $categoryId);

        $nextPage = $this->getContentByUrl($url, self::PATTERN_FOR_PAGE);

        if (!empty($nextPage[0])) {
            $this->setProductsFromAllPages(\sprintf(self::URL_SITE, $nextPage[1][0]), $categoryId);
        }
    }

    /**
     * @param array $htmlProducts
     * @param int $categoryId
     */
    private function setProducts(array $htmlProducts, int $categoryId): void
    {
        foreach ($htmlProducts[1] as $item => $name) {
            $product = new Product();

            $existingProduct = Product::getProductByVendorCode($htmlProducts[2][$item]);

            if ($existingProduct) {
                $product->setId($existingProduct['product_id']);
                $product->setSource($this->url);
            } else {
                $product->setId(++$this->maxProductId['max']);
                $product->setName($name);
                $product->setDateAvailability(\date('Y-m-d'));
                $product->setCategoryId($this->getCategoryByProductNameAndParentId($name, $categoryId));
                $product->setDescription('');
                $product->setSource($this->url);

                $seoUrl = ConversionToSeoUrl::conversion($name);
                $product->setSeoUrl(\sprintf(self::SEO_URL, $seoUrl, $product->getId()));
            }

            if (\strpos($htmlProducts[3][$item], 'В наличии') === false) {
                $stockStatusId = self::STATUS_ID_FOR_PRODUCT_OUT_OF_STOCK;
                $product->setQuantity(0);
            } else {
                $stockStatusId = self::STATUS_ID_FOR_PRODUCT_IN_STOCK;
                $product->setQuantity(100);
            }

            $product->setVendorCode($htmlProducts[2][$item]);
            $product->setStockStatusId($stockStatusId);
            $product->setPictures($this->getImage());

            $price = (int) $htmlProducts[4][$item];
            $priceWithMarkup = $price * $this->markup;
            $roundedPrice = \round($priceWithMarkup / 5) * 5;

            $product->setPrice($roundedPrice < 20 ? 20 : $roundedPrice);

            $product->saveInDataBase();

            echo \sprintf(self::DISPLAY_PRODUCT, $name, $product->getId());
        }
    }

    /**
     * @return array
     */
    private function getImage(): array
    {
        $images = [''];

        if ($this->parentIdForImage === 14732) {
            $images = [
                'https://cdn.ddaudio.com.ua/assets/cache/images/Skoda/shkoda-oktaviya-590x393-11a.jpg',
                'https://akl.by/media/files/products/6573/1665123.jpg',
                'http://sales24.by/wp-content/uploads/renault_espace_11_2002-on_modelnye_kovry_rezinovye_chernye_4_sht.jpg',
                'https://avtoplus.com.ua/content/images/21/1016986-rezinovye-kovriki-v-salon-opel-astra-h-2004-stingray-70431688634885_small11.jpg',
            ];
        } elseif ($this->parentIdForImage === 15613) {
            $images = [
                'https://images.by.prom.st/109930987_kovrik-v-bagazhnik.jpg',
            ];
        } elseif ($this->parentIdForImage === 21509 || $this->parentIdForImage === 21510) {
            $images = [
                'https://avtoshara.kiev.ua/productimage/300/300/univers/universal-cobratuning.jpg',
            ];
        } elseif ($this->parentIdForImage === 17012) {
            $images = [
                'http://vazistaz.com.ua/image/data/photo/2/-image-data-products-avtomobilnie-kovriki-v-salon-AVTM-vors-avtomobilnie-kovriki-v-salon-AVTM-vors.jpg',
            ];
        }

        $image = \array_rand($images, 1);

        return [$images[$image]];
    }

    /**
     * @param string $name
     * @param int $categoryId
     * @return int
     */
    private function getCategoryByProductNameAndParentId(string $name, int $categoryId): int
    {
        $categories = Category::getCategoriesByParentCategoryId($categoryId);

        foreach ($categories as $category) {
            if (\stripos($name, $category['name']) !== false) {
                return (int) $category['category_id'];
            }
        }

        return $categoryId;
    }
}