<?php

namespace Parsers;

use Components\ConversionToSeoUrl;
use Components\Rate;
use Interfaces\ParserInterface;
use Models\Category;
use Models\Product;

class AvtopilotParser implements ParserInterface
{
    private const STATUS_ID_FOR_PRODUCT_IN_STOCK = 7;
    private const STATUS_ID_FOR_PRODUCT_OUT_OF_STOCK = 8;
    private const PATTERN_FOR_DOCTYPE_IN_XML_FILE = '@(yml_catalog SYSTEM "shops.dtd")@';
    private const REPLACE_FOR_DOCTYPE = 'root [ <!ENTITY nbsp ""> ]';
    private const SEO_URL = '%s-%s';
    private const DISPLAY_CATEGORY = "category %s id %s\n";
    private const DISPLAY_PRODUCT = "product %s id %s\n";
    private const PATTERN_FOR_EQUIPMENT = '@Состав комплекта[\w\W]*?content_wrap">([\w\W]*?)</div[\w\W]*?Комплектация[\w\W]*?content_wrap">[\s]+([\w\W]*?)</div@';

    private $maxCategoryId;
    private $maxProductId;
    private $xmlContent;
    private $urlId;
    private $markup;
    private $url;

    /**
     * @param array $url
     */
    public function parseContentByUrl(array $url): void
    {
        $this->maxCategoryId = Category::getMaxId();
        $this->maxProductId = Product::getMaxId();
        $this->urlId = $url['id'];
        $this->markup = $url['markup'];
        $this->url = $url['url'];
        $contextOptions = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
            ],
        ];

        $content = \file_get_contents($url['url'], false, stream_context_create($contextOptions));
        $xml = \preg_replace(self::PATTERN_FOR_DOCTYPE_IN_XML_FILE, self::REPLACE_FOR_DOCTYPE, $content);
        $this->xmlContent = \simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOENT);

        $this->setCategories($url['parent_id']);
        $this->setOffers();
    }

    /**
     * @param int $parentId
     */
    private function setCategories(int $parentId): void
    {
        foreach ($this->xmlContent->shop->categories->category as $item) {
            $category = new Category();

            if (\trim($item) === 'VW') {
                $item[0] = 'VOLKSWAGEN';
            } elseif (\trim($item) === 'Газель') {
                $parentId = 87;
            } elseif (\trim($item) === 'Чехлы универсальные АВТОПИЛОТ') {
                $parentId = 5;
            }

            $parentId = $item['parentId'] === null ? $parentId : (int) $item['parentId'];

            $existingCategory = Category::getCategoryByNameAndParentId(\trim($item), $parentId);

            if ($existingCategory) {
                $category->setId($existingCategory['category_id']);
            } else {
                $category->setId((int) $item['id']);
                $category->setParentId($parentId);
                $category->setName(\trim($item));

                $seoUrl = ConversionToSeoUrl::conversion($item);
                $category->setSeoUrl(\sprintf(self::SEO_URL, $seoUrl, $item['id']));
            }

            $category->setImage('');

            $category->saveInDataBase();

            echo \sprintf(self::DISPLAY_CATEGORY, $item, $category->getId());
        }
    }

    private function setOffers(): void
    {
        foreach ($this->xmlContent->shop->offers->offer as $item) {
            $product = new Product();

            $existingProduct = Product::getProductByVendorCode($item->vendorCode);

            if ($existingProduct) {
                $product->setId($existingProduct['product_id']);
                $product->setSource($this->url);
            } else {
                $product->setId(++$this->maxProductId['max']);
                $product->setDateAvailability(\date('Y-m-d'));

                $productName = $product->getProductNameByVendorCode($item->vendorCode);
                $product->setName($productName ? $productName : $item->name);
                $product->setCategoryId((int) $item->categoryId);

                if ($this->urlId === 1) {
                    $description = $this->getEquipment($item->url, $item->description);
                } else {
                    $description = $item->description;
                }

                $product->setDescription($description);
                $product->setSource($this->url);

                $seoUrl = ConversionToSeoUrl::conversion($productName ? $productName : $item->name);
                $product->setSeoUrl(\sprintf(self::SEO_URL, $seoUrl, $item['id']));
            }

            $product->setQuantity((int) $item->quantity);

            $stockStatusId = (int) $item->quantity !== 0 ? self::STATUS_ID_FOR_PRODUCT_IN_STOCK : self::STATUS_ID_FOR_PRODUCT_OUT_OF_STOCK;
            $product->setStockStatusId($stockStatusId);
            $product->setVendorCode($item->vendorCode);

            $endOfVendorCode = $product->getEndOfVendorCode($item->vendorCode);

            if (!$endOfVendorCode || !$product->getProductImageByVendorCode($endOfVendorCode)) {
                $pictures = [(string) $item->picture];
                $product->setPictures($pictures);
            }

            $price = (int) \filter_var($item->price, FILTER_SANITIZE_NUMBER_INT);
            $priceWithMarkup = $price * $this->markup;
            $priceBYN = \round($priceWithMarkup * Rate::getRussianRubleRate() / 5) * 5;

            $product->setPrice($priceBYN < 20 ? 20 : $priceBYN);

            $product->saveInDataBase();

            echo \sprintf(self::DISPLAY_PRODUCT, $item->name, $product->getId());
        }
    }

    /**
     * @param $url
     * @param $description
     * @return string
     */
    private function getEquipment($url, $description): string
    {
        $url = \str_replace('http', 'https', $url);
        $this->authorization($url);
        $equipment = $this->getContentByUrl($url, self::PATTERN_FOR_EQUIPMENT);

        $description = "$description<br><br>" . 'Состав комплекта:<br>' . \trim($equipment[1][0], '\t') . '<br><br>' . 'Комплектация:<br>' . \trim($equipment[2][0], '\t');

        return $description;
    }

    /**
     * @param string $url
     */
    private function authorization(string $url)
    {
        $ch = \curl_init();
        \curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        \curl_setopt($ch, CURLOPT_URL, $url);
        \curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');
        \curl_setopt($ch, CURLOPT_POST, 1);
        \curl_setopt($ch, CURLOPT_POSTFIELDS, [
            'AUTH_FORM' => 'Y',
            'TYPE' => 'AUTH',
            'backurl' => '/auth/',
            'USER_LOGIN' => 'zakaz@100chehlov.by',
            'USER_PASSWORD' => '123456',
            'license' => 'on',
            'Login' => ''
        ]);
        \curl_setopt($ch, CURLOPT_HEADER, 1);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        \curl_setopt($ch, CURLOPT_COOKIEJAR, ROOT.'/cookie.txt');
        \curl_exec($ch);
        \curl_close($ch);
    }

    /**
     * @param string $url
     * @param string $pattern
     * @return array
     */
    private function getContentByUrl(string $url, string $pattern): array
    {
        $ch = \curl_init();
        \curl_setopt($ch, CURLOPT_URL, $url);
        \curl_setopt($ch, CURLOPT_POST, 0);
        \curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        \curl_setopt($ch, CURLOPT_HEADER, 1);
        \curl_setopt($ch, CURLOPT_COOKIEFILE, ROOT.'/cookie.txt');
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $content = \curl_exec($ch);
        \curl_close($ch);

        \preg_match_all($pattern, $content, $result, PREG_PATTERN_ORDER);

        return $result;
    }
}
