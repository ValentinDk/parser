<?php

namespace Parsers;

use Components\Rate;
use Interfaces\ParserInterface;
use Models\Category;
use Components\ConversionToSeoUrl;
use Models\Product;

class AvtoliderParser implements ParserInterface
{
    private const URL_SITE = 'https://avtolider1.ru%s';
    private const PATTERN_FOR_CATEGORIES = '@href="([\/\w]+)"[>\s<\w="]+([\/\w-.]+)"[>\/<\w\s]+<span>(.+)<\/span>@';
    private const PATTERN_FOR_EQUIPMENT = '@offer-url="([\/\w]+)\??">[\s]+[\w<"= ]+>(.+)<\/span>@';
    private const PATTERN_FOR_CATEGORY_NAME_IF_NO_EQUIPMENT = '@class="complect_single">[\s]+<div>(.+)<\/div>@';
    private const PATTERN_FOR_PAGE = '@class="page_item"[\w\s="]+([\/\w?=]+)"@';
    private const PATTERN_FOR_DEMO_PRODUCTS = '@name_elem"><a href="([\w\/]+)">[\w\s<>/\-.:(),+]+<span>(.+?)<\/@u';
    private const PATTERN_FOR_PRODUCTS = '@offer-name="true">(.*?)</[\W\w]+?offer-[\w]+="true">(.*?)<[\W\w]+?price="(\d+)">[\w\W]*?tab_cont">([\w\W]*?)<div class="item_tab"@';
    private const PATTERN_FOR_IMAGES_PRODUCT = '@<a href="(.+)" data-gallery@';
    private const STATUS_ID_FOR_PRODUCT_IN_STOCK = 7;
    private const STATUS_ID_FOR_PRODUCT_OUT_OF_STOCK = 8;
    private const SEO_URL = '%s-%s';
    private const DISPLAY_CATEGORY = "category %s id %s\n";
    private const DISPLAY_PRODUCT = "product %s id %s\n";

    private $maxCategoryId;
    private $maxProductId;
    private $markup;
    private $quantityProduct;
    private $urlsForImages;
    private $url;

    /**
     * @param array $url
     */
    public function parseContentByUrl(array $url): void
    {
        $this->maxCategoryId = Category::getMaxId();
        $this->maxProductId = Product::getMaxId();
        $this->markup = $url['markup'];
        $this->url = $url['url'];

        $this->parse($url['url'], $url['parent_id']);
    }

    /**
     * @param string $url
     * @param int $parentId
     */
    private function parse(string $url, int $parentId): void
    {
        $this->setCategories($url, $parentId);
        $this->setProductsByEquipments($url, $parentId);
    }

    /**
     * @param string $url
     * @param int $parentId
     */
    private function setCategories(string $url, int $parentId): void
    {
        $htmlCategories = $this->getContentByUrl($url, self::PATTERN_FOR_CATEGORIES);

        foreach ($htmlCategories[1] as $item => $categoryUrl) {
            $image = \sprintf(self::URL_SITE, $htmlCategories[2][$item]);
            $category = $this->createCategory($htmlCategories[3][$item], $parentId, $image);

            $this->parse(\sprintf(self::URL_SITE, $categoryUrl), $category->getId());
        }
    }

    /**
     * @param string $url
     * @param string $pattern
     * @return array
     */
    private function getContentByUrl(string $url, string $pattern): array
    {
        $ch = \curl_init();
        \curl_setopt($ch, CURLOPT_URL, $url);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $content = \curl_exec($ch);
        \curl_close($ch);

        \preg_match_all($pattern, $content, $result, PREG_PATTERN_ORDER);

        return $result;
    }

    /**
     * @param string $name
     * @param int $parentId
     * @param string $image
     * @return Category
     */
    private function createCategory(string $name, int $parentId, string $image = ''): Category
    {
        $category = new Category();

        if ($name === 'LADA (ВАЗ)') {
            $name = 'LADA';
        } elseif ($name === 'Mercedes Benz') {
            $name = 'Mercedes';
        } elseif ($name === 'Ssang Yong') {
            $name = 'SsangYong';
        } elseif ($name === 'УАЗ') {
            $name = 'UAZ';
        }

        $existingCategory = Category::getCategoryByNameAndParentId($name, $parentId);

        if ($existingCategory) {
            $category->setId($existingCategory['category_id']);
        } else {
            $category->setId(++$this->maxCategoryId['max']);
            $category->setParentId($parentId);
            $category->setName(\strtoupper($name));

            $seoUrl = ConversionToSeoUrl::conversion($name);
            $category->setSeoUrl(\sprintf(self::SEO_URL, $seoUrl, $category->getId()));
        }

        $category->setImage($image);

        $category->saveInDataBase();

        echo \sprintf(self::DISPLAY_CATEGORY, $name, $category->getId());

        return $category;
    }

    /**
     * @param string $url
     * @param int $categoryId
     */
    private function setProductsByEquipments(string $url, int $categoryId): void
    {
        $equipments = $this->getContentByUrl($url, self::PATTERN_FOR_EQUIPMENT);

        foreach ($equipments[2] as $equipment => $name) {
            $category = $this->createCategory($name, $categoryId);
            $category->setEquipment($category->getId());

            $this->setProductsFromAllPages(\sprintf(self::URL_SITE, $equipments[1][$equipment]), $category->getId());
        }

        $categoryName = $this->getContentByUrl($url, self::PATTERN_FOR_CATEGORY_NAME_IF_NO_EQUIPMENT);

        if (empty($equipments[0]) && !empty($categoryName[0])) {
            $category = $this->createCategory($categoryName[1][0], $categoryId);
            $category->setEquipment($category->getId());

            $this->setProductsFromAllPages($url, $category->getId());
        } else {
            $this->setProductsFromAllPages($url, $categoryId);
        }
    }

    /**
     * @param string $url
     * @param int $categoryId
     */
    private function setProductsFromAllPages(string $url, int $categoryId): void
    {
        $htmlDemoProducts = $this->getContentByUrl($url, self::PATTERN_FOR_DEMO_PRODUCTS);
        $this->setProductsByDemoProducts($htmlDemoProducts, $categoryId);

        $pages = $this->getContentByUrl($url, self::PATTERN_FOR_PAGE);

        foreach ($pages[1] as $item => $pageUrl) {
            $htmlDemoProducts = $this->getContentByUrl(\sprintf(self::URL_SITE, $pageUrl), self::PATTERN_FOR_DEMO_PRODUCTS);
            $this->setProductsByDemoProducts($htmlDemoProducts, $categoryId);
        }
    }

    /**
     * @param array $htmlDemoProducts
     * @param int $categoryId
     */
    private function setProductsByDemoProducts(array $htmlDemoProducts, int $categoryId): void
    {
        foreach ($htmlDemoProducts[1] as $item => $productUrl) {
            if (\strpos($htmlDemoProducts[2][$item], 'наличии') === false) {
                $this->quantityProduct = 0;
            } else {
                $this->quantityProduct = (int) \filter_var($htmlDemoProducts[2][$item], FILTER_SANITIZE_NUMBER_INT);
            }

            $this->urlsForImages = $this->getContentByUrl(\sprintf(self::URL_SITE, $productUrl), self::PATTERN_FOR_IMAGES_PRODUCT);
            $htmlProducts = $this->getContentByUrl(\sprintf(self::URL_SITE, $productUrl), self::PATTERN_FOR_PRODUCTS);

            $this->setProduct($htmlProducts, $categoryId);
        }
    }

    /**
     * @param array $htmlProducts
     * @param int $categoryId
     */
    private function setProduct(array $htmlProducts, int $categoryId): void
    {
        foreach ($htmlProducts[1] as $item => $name) {
            $product = new Product();

            $existingProduct = Product::getProductByVendorCode($htmlProducts[2][$item]);

            if ($existingProduct) {
                $product->setId($existingProduct['product_id']);
                $product->setSource($this->url);
            } else {
                $product->setId(++$this->maxProductId['max']);
                $product->setName($name);
                $product->setDateAvailability(\date('Y-m-d'));
                $product->setCategoryId($categoryId);
                $product->setDescription($htmlProducts[4][$item]);
                $product->setSource($this->url);

                $seoUrl = ConversionToSeoUrl::conversion($name);
                $product->setSeoUrl(\sprintf(self::SEO_URL, $seoUrl, $product->getId()));
            }

            $stockStatusId = $this->quantityProduct !== 0 ? self::STATUS_ID_FOR_PRODUCT_IN_STOCK : self::STATUS_ID_FOR_PRODUCT_OUT_OF_STOCK;
            $product->setStockStatusId($stockStatusId);

            $product->setVendorCode($htmlProducts[2][$item]);
            $product->setQuantity($this->quantityProduct);
            $product->setPictures($this->getImagesProduct());

            $price = (int) \filter_var($htmlProducts[3][$item], FILTER_SANITIZE_NUMBER_INT);
            $priceWithMarkup = $price * $this->markup;
            $priceBYN = \round($priceWithMarkup * Rate::getRussianRubleRate() / 5) * 5;

            $product->setPrice($priceBYN < 20 ? 20 : $priceBYN);

            $product->saveInDataBase();

            echo \sprintf(self::DISPLAY_PRODUCT, $name, $product->getId());
        }
    }

    /**
     * @return array
     */
    private function getImagesProduct(): array
    {
        $images = [];

        foreach ($this->urlsForImages[1] as $image) {
            $images[] = \sprintf(self::URL_SITE, $image);
        }

        return $images;
    }
}