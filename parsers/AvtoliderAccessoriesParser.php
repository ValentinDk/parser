<?php

namespace Parsers;

use Components\ConversionToSeoUrl;
use Components\Rate;
use Interfaces\ParserInterface;
use Models\Category;
use Models\Product;

class AvtoliderAccessoriesParser implements ParserInterface
{
    private const URL_SITE = 'https://avtolider1.ru%s';
    private const PATTERN_FOR_CATEGORIES_BASED_PRODUCTS = '@href="(.+)" [\w="/ ]+itemprop="url">[\s]+<span itemprop="title">(.+)</span>@u';
    private const PATTERN_FOR_CATEGORIES = '@href="([\/\w]+)"[>\s<\w="]+([\/\w-.]+)"[>\/<\w\s]+<span>(.+)<\/span>@';
    private const PATTERN_FOR_PAGE = '@class="page_item"[\w\s="]+([\/\w?=]+)"@';
    private const PATTERN_FOR_DEMO_PRODUCTS = '@name_elem"><a href="([\w\/]+)">[\w\W]*?</span>[\s]+<span>(.+?)<\/@';
    private const PATTERN_FOR_PRODUCTS = '@offer-name="true">(.*?)</[\W\w]+?offer-[\w]+="true">(.*?)<[\W\w]+?price="(\d+)">[\w\W]*?tab_cont">([\w\W]*?)<div class="item_tab"@';
    private const PATTERN_FOR_IMAGES_PRODUCT = '@<a href="(.+)" data-gallery@';
    private const STATUS_ID_FOR_PRODUCT_IN_STOCK = 7;
    private const STATUS_ID_FOR_PRODUCT_OUT_OF_STOCK = 8;
    private const SEO_URL = '%s-%s';
    private const DISPLAY_CATEGORY = "category %s id %s\n";
    private const DISPLAY_PRODUCT = "product %s id %s\n";

    private $maxCategoryId;
    private $maxProductId;
    private $markup;
    private $quantityProduct;
    private $urlsForImages;
    private $stockStatus;
    private $url;

    /**
     * @param array $url
     */
    public function parseContentByUrl(array $url): void
    {
        $this->maxCategoryId = Category::getMaxId();
        $this->maxProductId = Product::getMaxId();
        $this->markup = $url['markup'];
        $this->url = $url['url'];

        $categoryInfo = $this->createCategoryBasedProducts($url['url'], $url['parent_id']);
        $this->parse($categoryInfo[0], $categoryInfo[1]);
    }

    /**
     * @param string $url
     * @param int $parentId
     * @return array
     */
    private function createCategoryBasedProducts(string $url, int $parentId): array
    {
        $htmlCategoryBasedProducts = $this->getContentByUrl($url, self::PATTERN_FOR_CATEGORIES_BASED_PRODUCTS);
        $htmlCategory[] = \array_pop($htmlCategoryBasedProducts[1]);
        $htmlCategory[] = \array_pop($htmlCategoryBasedProducts[2]);

        $categoryBasedProducts = $this->createCategory($htmlCategory[1], $parentId);
        $categoryInfo = [$htmlCategory[0], $categoryBasedProducts->getId()];

        return $categoryInfo;
    }

    /**
     * @param string $url
     * @param int $parentId
     */
    private function parse(string $url, int $parentId): void
    {
        $this->setCategories($url, $parentId);
        $this->setProductsFromAllPages($url, $parentId);
    }

    /**
     * @param string $url
     * @param int $parentId
     */
    private function setCategories(string $url, int $parentId): void
    {
        $htmlCategories = $this->getContentByUrl(\sprintf(self::URL_SITE, $url), self::PATTERN_FOR_CATEGORIES);

        foreach ($htmlCategories[1] as $item => $categoryUrl) {
            $image = \sprintf(self::URL_SITE, $htmlCategories[2][$item]);
            $category = $this->createCategory($htmlCategories[3][$item], $parentId, $image);

            $this->parse($categoryUrl, $category->getId());
        }
    }

    /**
     * @param string $url
     * @param string $pattern
     * @return array
     */
    private function getContentByUrl(string $url, string $pattern): array
    {
        $ch = \curl_init();
        \curl_setopt($ch, CURLOPT_URL, $url);
        \curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $content = \curl_exec($ch);
        \curl_close($ch);

        \preg_match_all($pattern, $content, $result, PREG_PATTERN_ORDER);

        return $result;
    }

    /**
     * @param string $name
     * @param int $parentId
     * @param string $image
     * @return Category
     */
    private function createCategory(string $name, int $parentId, string $image = ''): Category
    {
        $category = new Category();

        $existingCategory = Category::getCategoryByNameAndParentId($name, $parentId);

        if ($existingCategory) {
            $category->setId($existingCategory['category_id']);
        } else {
            $category->setId(++$this->maxCategoryId['max']);
            $category->setParentId($parentId);
            $category->setName(\strtoupper($name));

            $seoUrl = ConversionToSeoUrl::conversion($name);
            $category->setSeoUrl(\sprintf(self::SEO_URL, $seoUrl, $category->getId()));
        }

        $category->setImage($image);

        $category->saveInDataBase();

        echo \sprintf(self::DISPLAY_CATEGORY, $name, $category->getId());

        return $category;
    }

    /**
     * @param string $url
     * @param int $categoryId
     */
    private function setProductsFromAllPages(string $url, int $categoryId): void
    {
        $htmlDemoProducts = $this->getContentByUrl(\sprintf(self::URL_SITE, $url), self::PATTERN_FOR_DEMO_PRODUCTS);
        $this->setProductsByDemoProducts($htmlDemoProducts, $categoryId);

        $pages = $this->getContentByUrl(\sprintf(self::URL_SITE, $url), self::PATTERN_FOR_PAGE);

        foreach ($pages[1] as $item => $pageUrl) {
            $htmlDemoProducts = $this->getContentByUrl(\sprintf(self::URL_SITE, $pageUrl), self::PATTERN_FOR_DEMO_PRODUCTS);
            $this->setProductsByDemoProducts($htmlDemoProducts, $categoryId);
        }
    }

    /**
     * @param array $htmlDemoProducts
     * @param int $categoryId
     */
    private function setProductsByDemoProducts(array $htmlDemoProducts, int $categoryId): void
    {
        foreach ($htmlDemoProducts[1] as $item => $productUrl) {
            if (\strpos($htmlDemoProducts[2][$item], 'наличии') === false) {
                $this->quantityProduct = 0;
                $this->stockStatus = self::STATUS_ID_FOR_PRODUCT_OUT_OF_STOCK;
            } elseif (\strpos($htmlDemoProducts[2][$item], 'наличии на складе') !== false) {
                $this->quantityProduct = 100;
                $this->stockStatus = self::STATUS_ID_FOR_PRODUCT_IN_STOCK;
            } else {
                $this->quantityProduct = (int) \filter_var($htmlDemoProducts[2][$item], FILTER_SANITIZE_NUMBER_INT);
                $this->stockStatus = self::STATUS_ID_FOR_PRODUCT_IN_STOCK;
            }

            $this->urlsForImages = $this->getContentByUrl(\sprintf(self::URL_SITE, $productUrl), self::PATTERN_FOR_IMAGES_PRODUCT);
            $htmlProducts = $this->getContentByUrl(\sprintf(self::URL_SITE, $productUrl), self::PATTERN_FOR_PRODUCTS);

            $this->setProduct($htmlProducts, $categoryId);
        }
    }

    /**
     * @param array $htmlProducts
     * @param int $categoryId
     */
    private function setProduct(array $htmlProducts, int $categoryId): void
    {
        foreach ($htmlProducts[1] as $item => $name) {
            $product = new Product();

            $existingProduct = Product::getProductByVendorCode($htmlProducts[2][$item]);

            if ($existingProduct) {
                $product->setId($existingProduct['product_id']);
                $product->setSource($this->url);
            } else {
                $product->setId(++$this->maxProductId['max']);
                $product->setName($name);
                $product->setDateAvailability(\date('Y-m-d'));
                $product->setCategoryId($categoryId);
                $product->setDescription($htmlProducts[4][$item]);
                $product->setSource($this->url);

                $seoUrl = ConversionToSeoUrl::conversion($name);
                $product->setSeoUrl(\sprintf(self::SEO_URL, $seoUrl, $product->getId()));
            }

            $product->setVendorCode($htmlProducts[2][$item]);
            $product->setQuantity($this->quantityProduct);
            $product->setStockStatusId($this->stockStatus);
            $product->setPictures($this->getImagesProduct());

            $price = (int) \filter_var($htmlProducts[3][$item], FILTER_SANITIZE_NUMBER_INT);
            $priceWithMarkup = $price * $this->markup;
            $priceBYN = \round($priceWithMarkup * Rate::getRussianRubleRate() / 5) * 5;

            $product->setPrice($priceBYN < 20 ? 20 : $priceBYN);

            $product->saveInDataBase();

            echo \sprintf(self::DISPLAY_PRODUCT, $name, $product->getId());
        }
    }

    /**
     * @return array
     */
    private function getImagesProduct(): array
    {
        $images = [];

        foreach ($this->urlsForImages[1] as $image) {
            $images[] = \sprintf(self::URL_SITE, $image);
        }

        return $images;
    }
}